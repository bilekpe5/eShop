package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;

/**
 * Auxiliary class for item storage
 */
public class ItemStock {
    private Item refItem;
    private int count;
    
    public ItemStock(Item refItem) {
        if(refItem==null) throw new IllegalArgumentException("Null refItem is not allowed!");
        this.refItem = refItem;
        count = 0;
    }
    
    @Override
    public String toString() {
        return "STOCK OF ITEM:  "+refItem.toString()+"    PIECES IN STORAGE: "+count;
    }

    public void IncreaseItemCount(int x) {
        if(x<0) throw new IllegalArgumentException("Increasing by negative number is not allowed!");
        count += x;
    }

    public void decreaseItemCount(int x) {
        if(x<0) throw new IllegalArgumentException("Decreasing by negative number is not allowed!");
        count -= x;
    }
    
    public int getCount() {
        return count;
    }
    
    public Item getItem() {
        return refItem;
    }
}