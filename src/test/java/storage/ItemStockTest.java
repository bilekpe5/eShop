package storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import cz.cvut.eshop.storage.ItemStock;
import org.junit.*;


import static junit.framework.TestCase.*;

public class ItemStockTest {

    @BeforeClass
    public static void beforeAll() {
        System.out.println("Starting tests for ItemStockTest");
    }

    @AfterClass
    public static void afterAll() {
        System.out.println("Finished");
    }

    @Test
    public void constructor_NotNullItem() {
        Item i = new StandardItem(1, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        assertNotNull(is.getItem());
    }

    @Test
    public void constructor_NotNullItem_ZeroCount() {
        Item i = new StandardItem(1, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        assertEquals(0, is.getCount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullItem() {
        Item i = null;
        ItemStock is = new ItemStock(i);
    }

    @Test
    public void toStringTest() {
        Item i = new StandardItem(1, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        String result = is.toString();
        String expected = "STOCK OF ITEM:  " + i.toString() + "    PIECES IN STORAGE: " + 0;
        assertEquals(expected, result);
    }

    @Test
    public void IncreaseItemCount_PositiveX() {
        Item i = new StandardItem(1, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        int before = is.getCount();
        int increment = 1;
        is.IncreaseItemCount(increment);
        int after = is.getCount();

        assertEquals(before + increment, after);
    }

    @Test(expected = IllegalArgumentException.class)
    public void IncreaseItemCount_NegativeX() {
        Item i = new StandardItem(1, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        int increment = -1;
        is.IncreaseItemCount(increment);
    }

    @Test
    public void DecreaseItemCount_PositiveX() {
        Item i = new StandardItem(1, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        int before = is.getCount();
        int decrement = 1;
        is.decreaseItemCount(decrement);
        int after = is.getCount();

        assertEquals(before - decrement, after);
    }

    @Test(expected = IllegalArgumentException.class)
    public void DecreaseItemCount_NegativeX() {
        Item i = new StandardItem(1, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        int decrement = -1;
        is.decreaseItemCount(decrement);
    }
}
