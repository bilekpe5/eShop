package storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import cz.cvut.eshop.storage.ItemStock;
import cz.cvut.eshop.storage.Storage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class StorageTest {

    @BeforeClass
    public static void beforeAll() {
        System.out.println("Starting tests for StorageTest");
    }

    @AfterClass
    public static void afterAll() {
        System.out.println("Finished");
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_WithArgument_NullStock() {
        Storage s = new Storage(null);
    }

    @Test
    public void constructor_Default() {
        Storage s = new Storage();
        assertNotNull(s);
    }

    @Test
    public void constructor_WithArgument() {
        HashMap<Integer, ItemStock> stock = new HashMap<>();
        Storage s = new Storage(stock);

        assertNotNull(s);
    }

    @Test
    public void printListOfStoredItems_EmptyStock() {
        Storage s = new Storage();
        s.printListOfStoredItems();
    }

    @Test
    public void printListOfStoredItems_FilledStock() {
        Item i = new StandardItem(1, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        HashMap<Integer, ItemStock> stock = new HashMap<>();
        stock.put(1, is);

        Storage s = new Storage(stock);
        s.printListOfStoredItems();
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertItems_NullItem() {
        Item i = null;
        Storage s = new Storage();

        s.insertItems(i, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertItems_NegativeCount() {
        int id = 1;

        Item i = new StandardItem(id, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        HashMap<Integer, ItemStock> stock = new HashMap<>();
        stock.put(id, is);

        Storage s = new Storage(stock);

        int increment = -1;
        s.insertItems(i, increment);
    }

    @Test
    public void insertItems_PositiveCount_NewItem() {
        int id = 1;

        Item i = new StandardItem(id, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        HashMap<Integer, ItemStock> stock = new HashMap<>();
        stock.put(id, is);

        Storage s = new Storage(stock);

        int before = stock.get(id).getCount();
        int increment = 1;
        s.insertItems(i, increment);

        int after = stock.get(id).getCount();

        assertEquals(after, before+increment);
    }

    @Test
    public void insertItems_PositiveCount_ExistingItem() {
        int id = 1;

        Item i = new StandardItem(id, "item1", 150, "category1", 150);
        ItemStock is = new ItemStock(i);

        HashMap<Integer, ItemStock> stock = new HashMap<>();
        stock.put(id, is);

        Storage s = new Storage(stock);
        int increment = 1;
        s.insertItems(i, increment);

        int before = stock.get(id).getCount();
        increment = 1;
        s.insertItems(i, increment);
        int after = stock.get(id).getCount();

        assertEquals(after, before+increment);
    }
}